function MongoReaderClient() {
    var baseUrl = "/MongoDbReader/";


    var makeRequest = function (successFunction,
                                errorFunction,
                                data,
                                url) {
        var requestUrl = baseUrl + url;
        $.ajax(requestUrl,
            {
                error: errorFunction,
                data: data
            }).done(successFunction);
    };

    // this function returns the lines available for analysis in the system
    this.getMetaData = function (successFunction,
                                 errorFunction) {
        // [lineID1, ... lineIDN]
        makeRequest(successFunction,
            errorFunction,
            {},
            "lineIds");
    };


    this.getStopsTable = function (lineId,
                                   startHour,
                                   endHour,
                                   startTime,
                                   endTime,
                                   successFunction,
                                   errorFunction) {
        makeRequest(successFunction,
            errorFunction,
            {
                lineId: lineId,
                startHour: startHour,
                endHour: endHour,
                startTime: startTime,
                endTime: endTime
            },
            "stopsTable");
    };
}

function Parameters() {

    this.setLineId = function (lineId) {
        console.log("requested update " + lineId);
        if (lineId) {
            this.lineId = lineId;
            this.refresh();
        }
    };

    this.setStartTime = function (time) {
        var oldVar = this.startTime;
        this.startTime = time;
        if (oldVar !== this.startTime)
            this.refresh();
    };

    this.setEndTime = function (time) {
        var oldVal = this.endTime;
        this.endTime = time;
        if (oldVal !== this.endTime)
            this.refresh();
    };

    this.setStartHour = function (startHour) {
        if (startHour >= 0) {
            this.startHour = parseInt(startHour);
            this.endHour = parseInt(startHour) + 1;
        }
        else {
            this.startHour = 0;
            this.endHour = 23;
        }
        this.refresh();
    };

    this.selectItinerary = function (itineraryId) {
        console.log(itineraryId);
        var nodeList = [];
        var itinerary = this.currentLineData.Itineraries.find(function (value) {
            return parseInt(value.Id) === parseInt(itineraryId);
        });
        if (itinerary) {
            for (var i = 0; i < itinerary.StopPointIds.length; i++) {
                var curId = itinerary.StopPointIds[i];
                var stopName;
                var numOfDevices;
                // try to recover number of devices
                var stopStatistic = this.stopsStatistics.find(function (value) {
                    return parseInt(value._id.stop_id) === parseInt(curId);
                });
                // if not found infer it
                if (!stopStatistic) numOfDevices = i === 0 ? 0 : nodeList[i - 1].numOfDevices;
                else {
                    numOfDevices = stopStatistic.num_of_devices_onboard;
                }
                // use id to recover name
                var stop = this.currentLineData.Stops.find(function (value) {
                    return parseInt(value.Id) === parseInt(curId);
                });
                stopName = stop.Name;

                nodeList.push(new Node(curId, stopName, Math.floor(numOfDevices)));
            }
        }
        else
            this.stopsStatistics.forEach(function (stopTableEntry) {
                nodeList.push(new Node(stopTableEntry._id.stop_id,
                    stopTableEntry._id.stop_id,
                    Math.floor(stopTableEntry.num_of_devices_onboard)));
            });

        d3.select("#now-showing")
            .attr("style", "display:block;")
            .text(function () {
                return "Now showing itinerary:" + itineraryId
            });

        createNodes("stop-canvas", nodeList);
    };

    this.setItineraryIds = function (itineraryIds) {
        var selfRef = this;
        this.itineraryIds = itineraryIds;
        var arrayIds = [];
        this.itineraryIds.forEach(function (id) {
            arrayIds.push(id);
        });

        var selector = "option:not(:first-child)";
        /*  var thisSel = d3.select("#itinerary-id-selection")
              .on("change", function () {
                  selfRef.selectItinerary(thisSel.property("value"));
              }); */
        var select = d3.select("#itinerary-id-selection")
            .selectAll(selector)
            .data(arrayIds);


        var itineraryOptionTextFunc = function (d) {
            var itineraries = selfRef.currentLineData.Itineraries;
            var result;
            for (var i = 0; i < itineraries.length; i++) {
                if (itineraries[i].Id === d) {
                    var currentItinerary = itineraries[i];
                    result = currentItinerary.Id.toString() + ": ";
                    var firstStop, lastStop;
                    firstStop = currentItinerary.StopPointIds[0];
                    lastStop = currentItinerary.StopPointIds[currentItinerary.StopPointIds.length - 1];
                    // now resolve stop names
                    var stops = selfRef.currentLineData.Stops;
                    var firstFound, secondFound;
                    firstFound = secondFound = false;
                    for (var j = 0; j < stops.length && !(firstFound && secondFound); j++) {
                        if (!firstFound && stops[j].Id === firstStop) {
                            firstStop = stops[j].Name;
                            firstFound = true;
                        }
                        if (!secondFound && stops[j].Id === lastStop) {
                            lastStop = stops[j].Name;
                            secondFound = true;
                        }
                    }
                    result += firstStop + " - " + lastStop;
                    break;
                }
            }
            if (result === undefined)
                result = d;
            return result;
        };

        select.attr("value", function (d) {
            return d;
        })
            .text(itineraryOptionTextFunc)
            .enter()
            .append("option")
            .attr("value", function (d) {
                return d;
            })
            .text(itineraryOptionTextFunc);

        select.exit()
            .remove();

        document.getElementById("itinerary-id-selection").selectedIndex = "0";
        d3.select("#itinerary-id-selection")
            .attr("class", ""); // clear invalid or refresh
        d3.select("#itinerary-id-selection")
            .attr("class", "refreshed");
    };

    this.refresh = function () {
        if (this.lineId &&
            typeof this.startHour !== "undefined" &&
            this.startHour >= 0 &&
            this.endHour) {
            d3.select("#itinerary-id-selection")
                .attr("class", "invalid");
            var client = new MongoReaderClient();
            var selfRef = this;
            client.getStopsTable(this.lineId,
                this.startHour,
                this.endHour,
                this.startTime,
                this.endTime,
                function (data) {
                    console.log(data);
                    selfRef.currentLineData = data.lineData.Data;
                    selfRef.stopsStatistics = data.stopsStatistics;
                    var itineraryIdsLineData = new Set();
                    for (var i = 0; i < selfRef.currentLineData.Itineraries.length; i++) {
                        itineraryIdsLineData.add(selfRef.currentLineData.Itineraries[i].Id);
                    }
                    var toRemove = [];
                    var itineraryIdsIntersection = new Set();
                    for (i = 0; i < selfRef.stopsStatistics.length; i++) {
                        if (! itineraryIdsLineData.has(selfRef.stopsStatistics[i]._id.itinerary_id)) {
                            toRemove.push(i);
                        }
                        else
                        {
                            itineraryIdsIntersection.add(selfRef.stopsStatistics[i]._id.itinerary_id);
                        }
                    }
                    toRemove.reverse();
                    for (var indexToRemove in toRemove)
                    {
                        selfRef.stopsStatistics.splice(indexToRemove, 1);
                    }
                    selfRef.setItineraryIds(itineraryIdsIntersection);
                },
                function () {
                    // TODO
                    console.log("error during stops error fetch");
                });

        }
    };
}

var globalParameters = new Parameters();


