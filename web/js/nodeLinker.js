Node = function (id, nodeName, numOfDevices) {
    this.id = id;
    this.name = nodeName;
    this.numOfDevices = numOfDevices;


};

function createNodes(svgElementId, nodeList) {
    console.log(nodeList);
    var GraphicalSettings = function () {
        this.lineLenght = 100;
        this.radius = 20;
        this.firstCenterOffset = 100;
        this.centersDistance = 2 * this.radius + this.lineLenght;
        this.minLineThickness = 1;
        this.maxLineThickness = 15;
        this.centersHeight = 50;
        this.width = function (dataLen) {
            return this.firstCenterOffset * 2 + this.centersDistance * dataLen;
        }
    };
    var params = new GraphicalSettings();
    var data = nodeList;

    var maxValue, minValue;
    maxValue = minValue = data[0].numOfDevices;

    data.forEach(function(item) {
        if (item.numOfDevices > maxValue)
            maxValue = item.numOfDevices;
        if (item.numOfDevices < minValue)
            minValue = item.numOfDevices;
    });

    var numOfPassScale = d3.scaleLinear()
        .domain([minValue, maxValue])
        .range([params.minLineThickness, params.maxLineThickness]);

    var canvas = d3.select("#" + svgElementId)
        .attr("width", params.width(data.length));

    canvas.selectAll("*").remove();

   /* var showDetails = function (stopData, i) {
        if (stopData)
        {
            d3.select("p#details")
                .selectAll("p")
                .data([])
                .exit()
                .remove();

            d3.select("p#details")
                .selectAll("p")
                .data([stopData])
                .enter()
                .append("p")
                .text(function(d) {return d.name})
                .append("p")
                .text(function(d) {return d.numOfDevices + " devices detected"});
        }
    }; */

    var showDetails = function() {return null;};

    var lines = canvas.selectAll("line")
        .data(data.slice(0, data.length - 1));

    var x1Func = function (d, i) {
        return (2 * params.radius + params.lineLenght) * i
            + params.firstCenterOffset
            + params.radius
            - 5
    };

    var x2Func = function (d, i) {
        return (2 * params.radius + params.lineLenght) * i
            + params.firstCenterOffset + params.radius
            + params.lineLenght
            + 5
    };

    var strokeWidthFunc = function(d) { return numOfPassScale(d.numOfDevices) };

    lines.attr("x1", x1Func)
        .attr("x2", x2Func)
        .attr("y1", params.centersHeight)
        .attr("y2", params.centersHeight)
        .attr("stroke", "red")
        .attr("stroke-width", strokeWidthFunc)
        .enter()
        .append("line")
        .attr("x1", x1Func)
        .attr("x2", x2Func)
        .attr("y1", params.centersHeight)
        .attr("y2", params.centersHeight)
        .attr("stroke", "red")
        .attr("stroke-width", strokeWidthFunc);
    lines.exit()
        .remove();

    var cxFunc = function(d, i) { return params.firstCenterOffset + i * params.centersDistance; };

    var circles = canvas.selectAll("circle")
        .data(data);

    circles.attr("cx", cxFunc)
        .attr("cy", params.centersHeight)
        .attr("r", params.radius)
        .enter()
        .append("circle")
        .attr("cx", cxFunc)
        .attr("cy", params.centersHeight)
        .attr("r", params.radius)
        .on("mouseover", showDetails);
    circles.exit()
        .remove();

    var stopClassName = "stopNames";
    var deviceCountClassName = "deviceCounts";
    canvas.append("g")
        .attr("class", stopClassName);

    canvas.append("g")
        .attr("class", deviceCountClassName);

    var stopNamesGroup = canvas.select("." + stopClassName).selectAll("text")
        .data(data);
    var deviceCounts = canvas.select("." + deviceCountClassName).selectAll("text")
        .data(data);

    stopNamesGroup.attr("x", function(d, i) { return params.firstCenterOffset + i * params.centersDistance; })
        .attr("y", params.centersHeight - params.radius - 10)
        .attr("text-anchor", "middle")
        .text(function(d) { return d.name; })
        .enter()
        .append("text")
        .attr("x", function(d, i) { return params.firstCenterOffset + i * params.centersDistance; })
        .attr("y", params.centersHeight - params.radius - 10)
        .attr("text-anchor", "middle")
        .text(function(d) { return d.name; });
    stopNamesGroup.exit()
        .remove();

    deviceCounts.attr("x", function(d, i) { return params.firstCenterOffset + i * params.centersDistance; })
        .attr("y", params.centersHeight + params.radius + 15)
        .attr("text-anchor", "middle")
        .text(function(d) { return Math.floor(d.numOfDevices).toString() + " devices"; })
        .enter()
        .append("text")
        .attr("x", function(d, i) { return params.firstCenterOffset + i * params.centersDistance; })
        .attr("y", params.centersHeight + params.radius + 15)
        .attr("text-anchor", "middle")
        .text(function(d) { return Math.floor(d.numOfDevices).toString() + " devices"; });
    deviceCounts.exit()
        .remove();

    d3.select("#canvas-container").attr("class", "refreshed");
}