{
    var animatedElemsId = ["#canvas-container", "#itinerary-id-selection"];
    animatedElemsId.forEach(function (value) {
        $(value).bind('oanimationend animationend webkitAnimationEnd', function () {
            $(this).removeClass("refreshed");
        });
    });


    var startDateOnChangeHandler = function () {
        var value = d3.select("#start-time-picker").property("value");
        var date = new Date(value);
        date.setHours(0, 0, 0, 0);
        globalParameters.setStartTime(date.getTime());
        d3.select("#end-time-picker").attr("min", value);
    };

    d3.select("#do-query")
        .on("click", function () {
            var selectedItinerary = d3.select("#itinerary-id-selection").property("value");
            if (selectedItinerary)
                globalParameters.selectItinerary(selectedItinerary);
        });

    var endDateOnChangeHandler = function () {
        var value = d3.select("#end-time-picker").property("value");
        var date = new Date(value);
        date.setHours(23, 59, 59, 0);
        globalParameters.setEndTime(date.getTime());
    };


    d3.select("#start-time-picker")
        .on("change", startDateOnChangeHandler);
    d3.select("#end-time-picker")
        .on("change", endDateOnChangeHandler);

    var onChange2 = function () {
        var elem = d3.select("#time-slot-selection");
        var value = elem.property("value");
        if (value)
            globalParameters.setStartHour(value);
    };

    var select2 = d3.select("#time-slot-selection")
        .on("change", onChange2);

    select2.selectAll("option:not(:first-child)")
        .data([-1, 6, 8, 10, 12, 14, 16, 18, 20, 22])
        .enter()
        .append("option")
        .attr("value", function (d) {
            return d;
        })
        .text(function (d) {
            if (d >= 0)
                return d + " - " + (d + 2) + " h";
            else
                return "All day";
        });

    var client = new MongoReaderClient();
    client.getMetaData(
        function (data) {
            data.sort(function (a, b) { return a - b; });
            var onChange1 = function () {
                var elem = d3.select("#line-selection");
                var value = elem.property("value");
                if (value) {
                    globalParameters.setLineId(value);
                }
            };


            var select1 = d3.select('#line-selection')
                .on('change', onChange1);

            select1.selectAll("option:not(:first-child)")
                .data(data)
                .enter()
                .append('option')
                .attr("value", function (d) {
                    return d;
                })
                .text(function (d) {
                    return d;
                });
        },
        function () {
            console.log("error");
        }
    );
}