<%--
  Created by IntelliJ IDEA.
  User: Francesco
  Date: 28/08/2018
  Time: 15:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <script src="js/jquery-3.3.1.js" type="text/javascript"></script>
    <script src="js/data.js" type="text/javascript"></script>
    <script src="js/nodeLinker.js" type="text/javascript"></script>
    <script src="js/d3.v5.js"></script>
</head>
<body>

<div id="selection-div">
    <p class="title">Query parameters</p>
    <div class="field">
        <label for="line-selection">Line ID</label>
        <select id="line-selection" class="semi-square">
            <option value="">Select line ID...</option>
        </select>
    </div>
    <div class="field">
        <label for="time-slot-selection">Time slot</label>
        <select id="time-slot-selection" class="semi-square">
            <option value="">Select a time slot...</option>
        </select>
    </div>
    <div class="field">
        <label for="itinerary-id-selection">Itinerary</label>
        <select id="itinerary-id-selection" class="semi-square">
            <option value="">Select an itinerary...</option>
        </select>
    </div>
    <div class="field">
        <label for="start-time-picker">From date</label>
        <input type="date" id="start-time-picker" class="semi-square">
    </div>
    <div class="field">
        <label for="end-time-picker">To date</label>
        <input type="date" id="end-time-picker" class="semi-square">
    </div>
    <div class="clear-fix">
        <button id="do-query" class="semi-square">Do query</button>
    </div>
</div>
<div id="now-showing" class="hidden">

</div>
<div id="canvas-container">
    <svg id="stop-canvas"></svg>
</div>

</body>

<script type="text/javascript" src="js/init.js"></script>
</html>