package main.java;

import com.mongodb.MongoClient;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.conversions.Bson;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.mongodb.client.model.Accumulators.avg;
import static com.mongodb.client.model.Aggregates.group;
import static com.mongodb.client.model.Aggregates.match;
import static com.mongodb.client.model.Filters.*;

public class StopsTableQuerier {

    // aggregate query params
    private final static String START_HOD_PARAM_NAME = "startHour";
    private final static String END_HOD_PARAM_NAME = "endHour";

    private final static String LINE_ID_PARAM_NAME = "lineId";

    // mongo db params
    private final static String STOPS_TABLE_DB_NAME = "stops_table";
    private final static String TIMESTAMP_MONGODB_FIELD_NAME = "timestamp";
    private final static String LINE_ID_MONGODB_FIELD_NAME = "line_id";
    private final static String HOUR_OF_DAY_MONGODB_FIELD_NAME = "hour_of_day";
    private final static String NUM_OF_DEVICE_MONGODB_FIELD_NAME = "$num_of_devices_onboard";
    private static final String START_TIME_PARAM_NAME = "startTime";
    private static final String END_TIME_PARAM_NAME = "endTime";
    private Long endTime;
    private Long startTime;
    private Long startHour;
    private Long endHour;
    private Integer lineId;

    public StopsTableQuerier(HttpServletRequest request) {
        String temp;
        startHour = request.getParameter(START_HOD_PARAM_NAME) == null ? null :
                Long.parseLong(request.getParameter(START_HOD_PARAM_NAME));
        endHour = request.getParameter(END_HOD_PARAM_NAME) == null ? null :
                Long.parseLong(request.getParameter(END_HOD_PARAM_NAME));
        lineId = request.getParameter(LINE_ID_PARAM_NAME) == null ? null :
                Integer.parseInt(request.getParameter(LINE_ID_PARAM_NAME));
        startTime = request.getParameter(START_TIME_PARAM_NAME) == null ? null :
                Long.parseLong(request.getParameter(START_TIME_PARAM_NAME));
        endTime = request.getParameter(END_TIME_PARAM_NAME) == null ? null :
                Long.parseLong(request.getParameter(END_TIME_PARAM_NAME));
    }

    /*
        Document whole = new Document();
        whole.put("stop_id", "$stop.id");
        whole.put("itinerary_id", "$itinerary_id");
        whole.put("hour_of_day", "$hour_of_day");

        AggregateIterable<Document> average = collection.aggregate(Arrays.asList(
                Aggregates.match(Filters.and(
                        Filters.gte(HOUR_OF_DAY_MONGODB_FIELD_NAME, startHour),
                        Filters.lte(HOUR_OF_DAY_MONGODB_FIELD_NAME, endHour)
                )),
                Aggregates.group(
                        whole.toBsonDocument(BsonDocument.class,
                                MongoClient.getDefaultCodecRegistry()),
                        Accumulators.avg("average", NUM_OF_DEVICE_MONGODB_FIELD_NAME)
                )
        ));*/


    public List<Document> doQuery(MongoClient mongoClientInstance) {
        MongoDatabase database = mongoClientInstance.getDatabase(STOPS_TABLE_DB_NAME);
        String collectionName = Integer.toString(lineId);
        MongoCollection<org.bson.Document> collection = database.getCollection(collectionName);
        List<Bson> aggregationQuery;
        if (endTime == null && startTime == null)
            aggregationQuery = Arrays.asList(
                    match(
                            and(
                                    gte(HOUR_OF_DAY_MONGODB_FIELD_NAME, startHour),
                                    lte(HOUR_OF_DAY_MONGODB_FIELD_NAME, endHour))),
                    group(
                            and(
                                    eq("itinerary_id",
                                            "$itinerary_id"),
                                    eq("stop_id",
                                            "$stop.id")),
                            // eq("hour_of_day",
                            //         "$hour_of_day")),
                            avg("num_of_devices_onboard",
                                    "$num_of_devices_onboard")));
        else if (endTime != null && startTime == null)
            aggregationQuery = Arrays.asList(
                    match(
                            and(
                                    gte(HOUR_OF_DAY_MONGODB_FIELD_NAME, startHour),
                                    lte(HOUR_OF_DAY_MONGODB_FIELD_NAME, endHour),
                                    lte(TIMESTAMP_MONGODB_FIELD_NAME, endTime))),
                    group(
                            and(
                                    eq("itinerary_id",
                                            "$itinerary_id"),
                                    eq("stop_id",
                                            "$stop.id")),
                            // eq("hour_of_day",
                            //         "$hour_of_day")),
                            avg("num_of_devices_onboard",
                                    "$num_of_devices_onboard")));
        else if (startTime != null && endTime == null)
            aggregationQuery = Arrays.asList(
                    match(
                            and(
                                    gte(HOUR_OF_DAY_MONGODB_FIELD_NAME, startHour),
                                    lte(HOUR_OF_DAY_MONGODB_FIELD_NAME, endHour),
                                    gte(TIMESTAMP_MONGODB_FIELD_NAME, startTime))),
                    group(
                            and(
                                    eq("itinerary_id",
                                            "$itinerary_id"),
                                    eq("stop_id",
                                            "$stop.id")),
                            // eq("hour_of_day",
                            //         "$hour_of_day")),
                            avg("num_of_devices_onboard",
                                    "$num_of_devices_onboard")));
        else
            aggregationQuery = Arrays.asList(
                    match(
                            and(
                                    gte(HOUR_OF_DAY_MONGODB_FIELD_NAME, startHour),
                                    lte(HOUR_OF_DAY_MONGODB_FIELD_NAME, endHour),
                                    gte(TIMESTAMP_MONGODB_FIELD_NAME, startTime),
                                    lte(TIMESTAMP_MONGODB_FIELD_NAME, endTime))),
                    group(
                            and(
                                    eq("itinerary_id",
                                            "$itinerary_id"),
                                    eq("stop_id",
                                            "$stop.id")),
                            // eq("hour_of_day",
                            //         "$hour_of_day")),
                            avg("num_of_devices_onboard",
                                    "$num_of_devices_onboard")));
        AggregateIterable<Document> queryResult = collection.aggregate(aggregationQuery);
        List<Document> result = new ArrayList<>();
        for (Document document : queryResult) {
            result.add(document);
        }
        return result;
    }

    public boolean isValidRequest() {
        return lineId != null && startHour != null &&
                endHour != null &&
                endHour > startHour;
    }

    public Integer getLineId() {
        return lineId;
    }
}
