package main.java;

import com.github.cliftonlabs.json_simple.JsonArray;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LineIdsReader extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try (MongoClient mongoClient = new MongoClient()) {
            response.setContentType("application/json");
            response.setStatus(200);
            MongoDatabase stops_table = mongoClient.getDatabase("stops_table");
            JsonArray jsonArray = new JsonArray();
            for (String listCollectionName : stops_table.listCollectionNames()) {
                jsonArray.add(listCollectionName);
            }
            ServletOutputStream outputStream = response.getOutputStream();
            outputStream.print(jsonArray.toJson());
            outputStream.flush();
        }
    }
}
