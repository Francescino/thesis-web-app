package main.java;

import com.github.cliftonlabs.json_simple.JsonArray;
import com.github.cliftonlabs.json_simple.JsonException;
import com.github.cliftonlabs.json_simple.JsonObject;
import com.github.cliftonlabs.json_simple.Jsoner;
import com.mongodb.MongoClient;
import org.bson.Document;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class StopsTableReader extends HttpServlet {
    @Override
    public void init() throws ServletException {
        super.init();
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    @Override
    public void destroy() {
        super.destroy();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        MongoClient mongoClient = new MongoClient();
        try {

            StopsTableQuerier queryExecutor = new StopsTableQuerier(request);

            response.setCharacterEncoding("UTF-8");
            if (!queryExecutor.isValidRequest()) {
                response.setContentType("text/html");
                response.setStatus(400);
                PrintWriter writer = response.getWriter();
                writer.println("Missing Parameters");
                writer.flush();
                return;
            }


            List<Document> stopsStatistics = queryExecutor.doQuery(mongoClient);
            if (stopsStatistics == null)
                stopsStatistics = new ArrayList<>();

            JsonArray stopsStatisticsJSON = new JsonArray();
            JsonObject cur;
            for (Document stopsStatistic : stopsStatistics) {
                cur = new JsonObject();
                for (Map.Entry<String, Object> stringObjectEntry : stopsStatistic.entrySet()) {
                    cur.put(stringObjectEntry.getKey(), stringObjectEntry.getValue());
                }
                stopsStatisticsJSON.add(cur);
            }

            // make request to mobility API
            URL url = new URL("https://www.oise-mobilite.fr/api/map/GetLineMap?lineId=" +
                    Integer.toString(queryExecutor.getLineId()));
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setRequestProperty("Accept", "application/json");
            httpURLConnection.connect();

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(httpURLConnection.getInputStream(),
                            StandardCharsets.UTF_8));
            String inputLine;
            StringBuilder stringResponse = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                stringResponse.append(inputLine);
            }
            in.close();

            JsonObject result = new JsonObject();
            result.put("stopsStatistics", stopsStatisticsJSON);
            JsonObject deserialize = (JsonObject)
                    Jsoner.deserialize(stringResponse.toString());
            result.put("lineData", deserialize);
            response.setContentType("application/json");
            response.setStatus(200);
            PrintWriter writer = response.getWriter();
            writer.println(result.toJson());
            writer.flush();
        } catch (JsonException e) {
            e.printStackTrace();
        } finally {
            mongoClient.close();
        }
    }

}
